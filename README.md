# graphql-typed-apollo-client [![npm version](https://img.shields.io/npm/v/graphql-typed-apollo-client.svg?style=flat-square)](https://www.npmjs.com/package/graphql-typed-apollo-client) [![Build Status](https://img.shields.io/travis/helios1138/graphql-typed-apollo-client/master.svg?style=flat-square)](https://travis-ci.org/helios1138/graphql-typed-apollo-client)

Cloned from graphql-typed-client@1.7.4 but export the function of converting query / mutation object to graphql AST also, so that application can integrate this library with other graphql client libraries like Apollo client.

Refer to original readme for details:  
https://www.npmjs.com/package/graphql-typed-client
